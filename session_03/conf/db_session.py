from curses import echo
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.future.engine import Engine
from pathlib import Path
from typing import Optional

from models.model_base import ModelBase

__engine: Optional[Engine] = None


def create_engine(sqlite: bool = False) -> Engine:
    global __engine

    if __engine:
        return
    
    if sqlite:
        file_db = 'db/db.sqlite'
        folder = Path(file_db).parent
        folder.mkdir(parents=True, exist_ok=True)
        
        connect_str = f'sqlite:///{file_db}'
        
        __engine = sa.create_engine(url=connect_str, echo=False, connect_args={'check_same_thread': False})
    else:
        connect_str = f'postgresql://sqlalchemy:sqlalchemy@localhost:5433/picoles'
        #               Nome do db | usuario   | senha    | porta        | nome do banco
        __engine = sa.create_engine(url=connect_str, echo=False)
            

def create_session() -> Session:
    global __engine

    if not __engine:
        create_engine()
    
    __session = sessionmaker(__engine, expire_on_commit=False, class_=Session)
    session: Session = __session()
    
    return session


def create_tables() -> None:
    global __engine

    if not __engine:
        create_engine()
    
    import models.__all_models
    ModelBase.metadata.drop_all(__engine)
    ModelBase.metadata.create_all(__engine)