from socket import AddressInfo
from typing import List

from sqlalchemy import func

from conf.helpers import formata_data
from conf.db_session import create_session

from models.models import Additive, Flavor, Dealer, Popsicle


def get_all_additives() -> None:
    with create_session() as session:
        # additives: List[Additive] = session.query(Additive) 
        additives: List[Additive] = session.query(Additive).all() # Traz uma lista
        for additive in additives:
            print(f'ID: {additive.id}')


def get_filter_flavor(flavor_id: int) -> None:
    with create_session() as session:
        # flavor: Flavor = session.query(Flavor).filter(Flavor.id==flavor_id).first()
        # flavor: Flavor = session.query(Flavor).filter(Flavor.id==flavor_id).one_or_none() 
        flavor: Flavor = session.query(Flavor).filter(Flavor.id==flavor_id).one() 

        print(flavor.name)


def get_all_popsicles() -> None:
    with create_session() as session:
        popsicle: List[Popsicle] = session.query(Popsicle).all()
        for pop in popsicle:
            print(pop)


def select_order_by_flavor() -> None:
    with create_session() as session:
        flavors: List[Flavor] = session.query(Flavor).order_by(Flavor.created_at.desc()).all()
        for flavor in flavors:
            print(flavor)


def select_group_by_popsicle() -> None:
    with create_session() as session:
        popsicle: List[Popsicle] = session.query(Popsicle).group_by(Popsicle.id, Popsicle.type_popsicle_id).all()
        for pop in popsicle:
            print(pop)


def select_flavor_limit() -> None:
    with create_session() as session:
        flavors: List[Flavor] = session.query(Flavor).limit(3)
        for flavor in flavors:
            print(flavor.id)

def select_count_dealers() -> None:
    with create_session() as session:
        dealer_quantity: Dealer = session.query(Dealer).count()
        
        print(dealer_quantity)

def select_count_aggregate() -> None:
    with create_session() as session:
        result: List = session.query(
                func.sum(Popsicle.price).label('sum'),
                func.avg(Popsicle.price).label('avg'),
                func.min(Popsicle.price).label('min'),
                func.max(Popsicle.price).label('max'),
            )
        
        print(f'Sum: {result[0][0]} | Avg: {result[0][1]} |\n\
                Min: {result[0][2]} | Max: {result[0][3]} |')
    
if __name__ == '__main__':
    # get_all_additives()
    # get_filter_flavor(1)
    # get_all_popsicles()
    # select_order_by_flavor()
    # select_group_by_popsicle()
    select_flavor_limit()
    select_count_dealers()
    select_count_aggregate()