from conf.db_session import create_session

from models.models import Flavor, Popsicle


def update_flavor(flavor_id: int, new_name: str) -> Flavor:
    with create_session() as session:
        flavor: Flavor = session.query(Flavor).filter(Flavor.id == flavor_id).first()

        if flavor:
            flavor.name = new_name
            session.commit()
            return flavor
        else:
            print('Não existe esse sabor')


def update_popsicle(popsicle_id: int, new_flavor_id: int, new_price: float) -> None:
    with create_session() as session:
        popsicle: Popsicle = session.query(Popsicle).filter(Popsicle.id == popsicle_id).first()

        if popsicle:
            popsicle.flavor_id = new_flavor_id
            popsicle.price = new_price
            session.commit()
            return popsicle
        else:
            print('Não existe picolé com esse ID')
        


if __name__ == '__main__':
    flavor = update_flavor(flavor_id=1, new_name='Novo sabor')
    update_popsicle(1, flavor.id, 50.99)