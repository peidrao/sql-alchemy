from typing import Optional

from conf.db_session import create_session
from models.models import Dealer, Popsicle


def delete_popsicle(popsicle_id: int) -> None:
    with create_session() as session:
        popsicle: Optional[Popsicle] = session.query(Popsicle).filter(Popsicle.id == popsicle_id).first()

        if popsicle:
            session.delete(popsicle)
            session.commit()
        else: 
            print('Não foi encontrado nenhum picolé.')

def delete_dealer(dealer_id: int) -> None:
    with create_session() as session:
        dealer: Optional[Dealer] = session.query(Dealer).filter(Dealer.id == dealer_id).first()

        if dealer:
            session.delete(dealer)
            session.commit()
        else: 
            print('Não foi encontrado nenhum revendedor.')


if __name__ == "__main__":
    # delete_popsicle(2)
    delete_dealer(2)