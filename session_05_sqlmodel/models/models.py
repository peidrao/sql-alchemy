from typing import Optional, List
from sqlmodel import Field, SQLModel, Relationship
from datetime import datetime
from sqlalchemy import UniqueConstraint

from pydantic import condecimal


class Additive(SQLModel, table=True):
    __tablename__: str = 'additions'
    __table_args__ = (UniqueConstraint('name'), UniqueConstraint('chemical_formula'))

    id: Optional[int] = Field(default=None, primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)
    chemical_formula: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<Additive: {self.name}>'


class Flavor(SQLModel, table=True):
    __tablename__: str = 'flavors'
    __table_args__ = (UniqueConstraint('name'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<Flavor: {self.name}>'


class PackingType(SQLModel, table=True):
    __tablename__: str = 'types_packing'
    __table_args__ = (UniqueConstraint('name'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<PackingType: {self.name}>'


class PopsicleType(SQLModel, table=True):
    __tablename__: str = 'types_popsicle'
    __table_args__ = (UniqueConstraint('name'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<PopsicleType: {self.name}>'


class Ingredient(SQLModel, table=True):
    __tablename__: str = 'ingredients'
    __table_args__ = (UniqueConstraint('name'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<Ingreditent: {self.name}>'


class Preservative(SQLModel, table=True):
    __tablename__: str = 'preservatives'
    __table_args__ = (UniqueConstraint('name'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)
    description: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<Preservative: {self.name}>'


class Dealer(SQLModel, table=True):
    __tablename__: str = 'dealers'
    __table_args__ = (UniqueConstraint('name'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)
    corporate_name: str = Field(max_length=45)
    contact: str = Field(max_length=45)

    def __repr__(self) -> str:
        return f'<Dealer: {self.name}>'


class Lot(SQLModel, table=True):
    __tablename__: str = 'lots'

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    types_popsicle_id: Optional[int] = Field(foreign_key='types_popsicle.id', default=None)
    types_popsicle: PopsicleType = Relationship(sa_relationship_kwargs={'lazy': 'joined'})
    quantity : int = Field()

    def __repr__(self) -> str:
        return f'<Lot: {self.id}>'


class LotsInvoice(SQLModel, table=True):
    id: Optional[int] = Field(primary_key=True)
    id_invoice: Optional[int] =Field(foreign_key='invoices.id')
    id_lots: Optional[int] =Field(foreign_key='lots.id')
    

class Invoice(SQLModel, table=True):
    __tablename__: str = 'invoices'
    __table_args__ = (UniqueConstraint('serial_number'),)

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    value: condecimal(max_digits=5, decimal_places=2) = Field(default=0)
    serial_number: str = Field(max_length=45)
    description: str = Field(max_length=45)

    dealer_id: Optional[int] = Field(foreign_key='dealers.id')
    dealer: Dealer = Relationship(sa_relationship_kwargs={'lazy': 'joined', 'cascade': 'delete'})

    lots: List[Lot] = Relationship(link_model=LotsInvoice, sa_relationship_kwargs={'lazy': 'dynamic'})

    def __repr__(self) -> str:
        return f'<Invoice: {self.id}>'


class IngredientPopsicles(SQLModel, table=True):
    id: Optional[int] = Field(primary_key=True)
    id_popsicle: Optional[int] = Field(foreign_key='popsicles.id')
    id_ingredient: Optional[int] = Field(foreign_key='ingredients.id')


class PreservativePopsicles(SQLModel, table=True):
    id: Optional[int] = Field(primary_key=True)
    id_popsicle: Optional[int] =Field(foreign_key='popsicles.id')
    id_preservative: Optional[int] =Field(foreign_key='preservatives.id')


class AdditivePopsicles(SQLModel, table=True):
    id: Optional[int] = Field(primary_key=True)
    id_popsicle: Optional[int] =Field(foreign_key='popsicles.id')
    id_additive: Optional[int] =Field(foreign_key='additions.id')


class Popsicle(SQLModel, table=True):
    __tablename__: str = 'popsicles'

    id: Optional[int] = Field(primary_key=True)
    created_at: datetime = Field(default=datetime.now(), index=True)
    name: str = Field(max_length=45)
    price: condecimal(max_digits=5, decimal_places=2) = Field(default=0)

    flavor_id: Optional[int] = Field(foreign_key='flavors.id')
    flavor: Flavor = Relationship(sa_relationship_kwargs={'lazy': 'joined'})

    type_popsicle_id: Optional[int] = Field(foreign_key='types_popsicle.id')
    type_popsicle: PopsicleType = Relationship(sa_relationship_kwargs={'lazy': 'joined'})

    type_packing_id: Optional[int] = Field(foreign_key='types_packing.id')
    type_packing: PackingType = Relationship(sa_relationship_kwargs={'lazy': 'joined'})

    ingredients: List[Ingredient] = Relationship(link_model=IngredientPopsicles, sa_relationship_kwargs={'lazy': 'joined'})
    preservatives: Optional[List[Preservative]] = Relationship(link_model=PreservativePopsicles, sa_relationship_kwargs={'lazy': 'joined'})
    additions: Optional[List[Additive]] = Relationship(link_model=AdditivePopsicles, sa_relationship_kwargs={'lazy': 'joined'})
    
    def __repr__(self) -> str:
        return f'<Popsicle: {self.id}>'
