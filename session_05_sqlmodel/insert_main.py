from random import randint
from conf.db_session import create_session
from models.models import Additive, Dealer, Flavor, Ingredient, Invoice, Lot, PackingType, Popsicle, PopsicleType, Preservative


def insert_additive() -> Additive:
    random = randint(1, 1_000)
    name: str = f'Name {random}'
    chemical_formula: str = f'Chemical {random}'
    additive: Additive = Additive(name=name, chemical_formula=chemical_formula)

    with create_session() as session:
        session.add(additive)
        session.commit()
    
        print(f'ID: {additive.id} | Created At: {additive.created_at} | Name: {additive.name}')
        return additive


def insert_flavor() -> Flavor:
    random = randint(1, 1_000)
    name: str = f'Flavor {random}'
    flavor: Flavor = Flavor(name=name)

    with create_session() as session:
        session.add(flavor)
        session.commit()
    
        return flavor


def insert_packing_type() -> PackingType:
    random = randint(1, 1_000)
    name: str = f'Packing Type: {random}'
    packing: PackingType = PackingType(name=name)

    with create_session() as session:
        session.add(packing)
        session.commit()
    
        print(f'ID: {packing.id} | Created At: {packing.created_at} | Name: {packing.name}')

        return packing


def insert_popsicle_type() -> PopsicleType:
    random = randint(1, 1_000)
    name: str = f'Popsicle Type: {random}'
    popsicle_type: PopsicleType = PopsicleType(name=name)

    with create_session() as session:
        session.add(popsicle_type)
        session.commit()
    
        return popsicle_type


def insert_ingredient() -> Ingredient:
    random = randint(1, 1_000)
    name: str = f'Ingredient: {random}'
    ingredient: Ingredient = Ingredient(name=name)

    with create_session() as session:
        session.add(ingredient)
        session.commit()
    
        print(f'ID: {ingredient.id} | Created At: {ingredient.created_at} | Name: {ingredient.name}')

        return ingredient


def insert_preservative() -> Preservative:
    random = randint(1, 1_000)
    name: str = f'Preservative - Name: {random}'
    description: str = f'Preservative - Description: {random}'
    preservative: Preservative = Preservative(name=name, description=description)

    with create_session() as session:
        session.add(preservative)
        session.commit()
    
        print(f'ID: {preservative.id} | Created At: {preservative.created_at} | Name: {preservative.name}')
        return preservative


def insert_dealer() -> Dealer:
    random = randint(1, 1_000)
    name: str = f'Dealer - Name: {random}'
    corporate_name: str = f'Dealer - Corporate Name: {random}'
    contact: str = f'Dealer - Contact: {random}'

    dealer: Dealer = Dealer(name=name, corporate_name=corporate_name, contact=contact)

    with create_session() as session:
        session.add(dealer)
        session.commit()
    
        return dealer


def insert_lot(type_popsice_id: int = None) -> Lot:
    random = randint(1, 1_000)
    quantity: int = random
    
    lot: Lot = Lot(quantity=quantity, types_popsicle_id=type_popsice_id)

    with create_session() as session:
        session.add(lot)
        session.commit()
    
        return lot


def insert_invoice(dealer_id: int, lot: Lot = None) -> Invoice:
    random = randint(1_000, 5_000)

    value: float = 1_000 + float(random)  
    description: str = f'New Invoice # {random}'
    serial_number: str = f'Serial # {random}'

    invoice: Invoice = Invoice(value=value, serial_number=serial_number, description=description, dealer_id=dealer_id)
    lot2 = insert_lot()
    invoice.lots.append(lot)
    invoice.lots.append(lot2)

    with create_session() as session:
        session.add(invoice)
        session.commit()
    
        return invoice


def insert_popsicle(flavor_id: int, packing_type_id: int, popsicle_type_id: int) -> None:
    random = randint(10, 35)

    price: float = 10 + float(random)  
    name: str = f'Flavor:  {random}'

    popsicle: Popsicle = Popsicle(price=price, name=name, flavor_id=flavor_id, type_packing_id=packing_type_id,
                                type_popsicle_id=popsicle_type_id)
    
    ingredient1 = insert_ingredient()
    ingredient2 = insert_ingredient()
    preservative1 = insert_preservative()
    preservative2 = insert_preservative()
    additive1 = insert_additive() 
    additive2 = insert_additive() 

    popsicle.ingredients.append(ingredient1)
    popsicle.ingredients.append(ingredient2)
    popsicle.preservatives.append(preservative1)
    popsicle.preservatives.append(preservative2)
    popsicle.additions.append(additive1)
    popsicle.additions.append(additive2)

    with create_session() as session:
        session.add(popsicle)
        session.commit()
    
        return popsicle


if __name__ == '__main__':
    insert_additive()
    flavor = insert_flavor()
    packing_type = insert_packing_type()
    popsicle_type = insert_popsicle_type()
    insert_ingredient()
    insert_preservative()
    dealer = insert_dealer()
    lot = insert_lot(popsicle_type.id)
    invoice = insert_invoice(dealer.id, lot)
    insert_popsicle(flavor.id, packing_type.id, popsicle_type.id)