from pathlib import Path
from typing import Optional
from sqlalchemy.future.engine import Engine

from sqlmodel import Session, SQLModel, create_engine as _create_engine

__engine: Optional[Engine] = None


def create_engine(sqlite: bool = False) -> Engine:
    global __engine
    
    if __engine:
        return

    if sqlite:
        file_db = 'db/popsicles.sqlite'
        folder = Path(file_db).parent
        folder.mkdir(parents=True, exist_ok=True)

        conn_str = f'sqlite:///{file_db}'
        __engine = _create_engine(url=conn_str, echo=False, connect_args={'check_same_thread': False})
    else:
        connect_str = f'postgresql://sqlalchemy:sqlalchemy@localhost:5433/picoles'
        __engine = _create_engine(url=connect_str, echo=False)
    
    return __engine
    
            

def create_session() -> Session:
    global __engine

    if not __engine:
        create_engine()
    
    session: Session = Session(__engine)
    
    return session


def create_tables() -> None:
    global __engine

    if not __engine:
        create_engine()
    
    import models.__all_models
    SQLModel.metadata.drop_all
    SQLModel.metadata.create_all