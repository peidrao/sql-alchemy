import asyncio
from typing import List

from sqlalchemy import func
from sqlalchemy.future import select
from conf.db_session import create_session

from models.models import Additive, Flavor, Dealer, Popsicle


async def get_all_additives() -> None:
    async with create_session() as session:
        query = select(Additive)
        additives: List[Additive] = await session.execute(query)
        additives = additives.scalars().all()
        for additive in additives:
            print(f'ID: {additive.id}')


async def get_filter_flavor(flavor_id: int) -> None:
    async with create_session() as session:
        query = select(Flavor).filter(Flavor.id == flavor_id)
        result = await session.execute(query)

        flavor: Flavor = result.scalars().first()
        print(flavor.id)


async def get_all_popsicles() -> None:
    async with create_session() as session:
        popsicles: List[Popsicle] = await session.execute(select(Popsicle))
        popsicles = popsicles.scalars().unique().all()
        
        for pop in popsicles:
            print(pop)


async def select_order_by_flavor() -> None:
    async with create_session() as session:
        flavors : List[Flavor] = await session.execute(select(Flavor).order_by(Flavor.created_at.desc()))
        flavors = flavors.scalars().all()
        for flavor in flavors:
            print(flavor.created_at)


async def select_group_by_popsicle() -> None:
    async with create_session() as session:
        popsicles: List[Popsicle] =  await session.execute(select(Popsicle).group_by(Popsicle.id, Popsicle.type_popsicle_id))
        popsicles = popsicles.scalars().unique().all()
        for pop in popsicles:
            print(pop)


async def select_flavor_limit() -> None:
    async with create_session() as session:
        flavors: List[Flavor] =  await session.execute(select(Flavor).limit(25))
        flavors = flavors.scalars().all()
        for flavor in flavors:
            print(flavor.id)


async def select_count_dealers() -> None:
    async with create_session() as session:
        dealer = await session.execute(func.count(Dealer.id))
        dealer_quantity: int = dealer.scalar()
        
        print(dealer_quantity)


async def select_count_aggregate() -> None:
    async with create_session() as session:
        query = select(
                func.sum(Popsicle.price).label('sum'),
                func.avg(Popsicle.price).label('avg'),
                func.min(Popsicle.price).label('min'),
                func.max(Popsicle.price).label('max'),
            )

        result = await session.execute(query)
        result = result.all()
        
        print(f'Sum: {result[0][0]} | Avg: {result[0][1]} |\n\
                Min: {result[0][2]} | Max: {result[0][3]} |')
    

if __name__ == '__main__':
    # asyncio.run(get_all_additives()) 
    # asyncio.run(get_filter_flavor(3))
    # asyncio.run(get_all_popsicles())
    # asyncio.run(select_order_by_flavor())
    # asyncio.run(select_group_by_popsicle())
    # asyncio.run(select_flavor_limit())
    # asyncio.run(select_count_dealers())
    asyncio.run(select_count_aggregate())