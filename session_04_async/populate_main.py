import asyncio
from importlib.resources import Package

from tqdm import tqdm  # pip install tqdm
from sqlalchemy.ext.asyncio import AsyncSession

from conf.helpers import gerar_string, gerar_int, gerar_float, gerar_cor
from conf.db_session import create_session
from models.models import Additive, Flavor, PackingType, PopsicleType, Ingredient, Preservative, Dealer, Lot, Invoice, Popsicle


#1) Aditivos Nutritivos
async def populate_aditivo_nutritivo():
    print(f'Cadastrando Aditivo Nutritivo: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
    
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        nome: str = gerar_string()
        chemical_formula: str = gerar_string(frase=True)

        aditivo_nutritivo: Additive = Additive(name=nome, chemical_formula=chemical_formula)
        session.add(aditivo_nutritivo)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Aditivos Nutritivos cadastrados com sucesso')

#2) Sabores
async def populate_sabor():
    print(f'Cadastrando Sabores: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        nome: str = gerar_string()

        sabor: Flavor = Flavor(name=nome)
        session.add(sabor)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Sabores cadastrados com sucesso')

#3) Tipos Embalagem
async def populate_tipo_embalagem():
    print(f'Cadastrando Tipos Embalagem: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        nome: str = gerar_string()

        tipo_embalagem: PackingType = PackingType(name=nome)
        session.add(tipo_embalagem)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Tipos Embalagem cadastrados com sucesso')


#4) Tipos Picole
async def populate_tipo_picole():
    print(f'Cadastrando Tipos Picolé: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        nome: str = gerar_string()

        tipo_picole: PopsicleType = PopsicleType(name=nome)
        session.add(tipo_picole)
        await asyncio.sleep(0.05)  
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Tipos Picolé cadastrados com sucesso')


#5) Ingredientes
async def populate_ingrediente():
    print(f'Cadastrando Ingredientes: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        nome: str = gerar_string()

        ingrediente: Ingredient = Ingredient(name=nome)
        session.add(ingrediente)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Ingredientes cadastrados com sucesso')

#6) Conservantes
async def populate_conservante():
    print(f'Cadastrando Conservantes: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
    
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        nome: str = gerar_string()
        descricao: str = gerar_string(frase=True)

        conservante: Preservative = Preservative(name=nome, description=descricao)
        session.add(conservante)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Conservantes cadastrados com sucesso')


#7) Revendedor
async def populate_revendedor():
    print(f'Cadastrando Revendedores: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        name: str = gerar_string()
        corporate_name: str = gerar_string()
        contact: str = gerar_string()

        revendedor: Dealer = Dealer(name=name, corporate_name=corporate_name, contact=contact)
        session.add(revendedor)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Revendedores cadastrados com sucesso')

#8) Lote
async def populate_lote():
    print(f'Cadastrando Lotes: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
    
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        id_tipo_picole: int = gerar_int()
        quantidade: int = gerar_int()

        lote: Lot = Lot(types_popsicle_id=id_tipo_picole, quantity=quantidade)
        session.add(lote)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Lotes cadastrados com sucesso')


#9) Nota Fiscal
async def populate_nota_fiscal():
    print(f'Cadastrando Notas Fiscais: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        valor: float = gerar_float(digitos=3)
        numero_serie: str = gerar_string()
        descricao: str = gerar_string(frase=True)
        id_revendedor: int = gerar_int()

        nota_fiscal: Invoice = Invoice(value=valor, serial_number=numero_serie, description=descricao, dealer_id=id_revendedor)
        session.add(nota_fiscal)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Notas Fiscais cadastradas com sucesso')


#10) Piole
async def populate_picole():
    print(f'Cadastrando Picolés: ')

    # Estamos criando a sessão antes pois vamos inserir vários objetos
    session: AsyncSession = create_session()
        
    cor = gerar_cor()
    for n in tqdm(range(1, 101), desc='Cadastrando...', colour=cor):
        preco: float = gerar_float()
        id_sabor: int = gerar_int()
        id_tipo_embalagem: int = gerar_int()
        id_tipo_picole: int = gerar_int()

        picole: Popsicle = Popsicle(price=preco, flavor_id=id_sabor, type_packing_id=id_tipo_embalagem, type_popsicle_id=id_tipo_picole)
        
        # Ingredientes
        for n in range(5):
            nome: str = gerar_string()
            ingrediente: Ingredient = Ingredient(name=nome)
            picole.ingredients.append(ingrediente)

        op = gerar_float()
        if op > 5:
            for _ in range(3):
                # Aditivos Nutritivos
                nome: str = gerar_string()
                chemical_formula: str = gerar_string(frase=True)
                aditivo_nutritivo: Additive = Additive(name=nome, chemical_formula=chemical_formula)
                picole.additions.append(aditivo_nutritivo)
        else:
            for _ in range(3):
                # Conservantes
                nome: str = gerar_string()
                descricao: str = gerar_string(frase=True)
                conservante: Preservative = Preservative(name=nome, description=descricao)
                picole.preservatives.append(conservante)
        session.add(picole)
        await asyncio.sleep(0.05)
    # Perceba que estamos executando o commit somente no final. Desta forma os 100 dados serão enviados em um único batch para o banco
    await session.commit()
    await session.close()
    print('Picolés cadastrados com sucesso')


async def popular():
    #1) Aditivos Nutritivos
    await populate_aditivo_nutritivo()

    #2) Sabores
    await populate_sabor()

    #3) Tipos Embalagem
    await populate_tipo_embalagem()

    #4) Tipos Picole
    await populate_tipo_picole()

    #5) Ingredientes
    await populate_ingrediente()

    #6) Conservantes (Deixando vazio para poder verificar resultados em tabelas vazias)
    # await populate_conservante()
    
    #7) Revendedores
    await populate_revendedor()

    #8) Lotes
    await populate_lote()

    #9) Notas Fiscais
    await populate_nota_fiscal()

    #10) Picole
    await populate_picole()


if __name__ == '__main__':
   asyncio.run(popular())
