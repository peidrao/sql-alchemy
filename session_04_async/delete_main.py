import asyncio
from typing import Optional
from sqlalchemy.future import select

from conf.db_session import create_session
from models.models import Dealer, Popsicle


async def delete_popsicle(popsicle_id: int) -> None:
    async with create_session() as session:
        popsicle: Optional[Popsicle] = await session.execute(select(Popsicle).filter(Popsicle.id == popsicle_id))
        popsicle = popsicle.unique().scalar_one_or_none()

        if popsicle:
            await session.delete(popsicle)
            await session.commit()
        else: 
            print('Não foi encontrado nenhum picolé.')

async def delete_dealer(dealer_id: int) -> None:
    async with create_session() as session:
        dealer: Optional[Dealer] = await session.execute(select(Dealer).filter(Dealer.id == dealer_id))
        dealer = dealer.scalar_one_or_none()

        if dealer:
            await session.delete(dealer)
            await session.commit()
        else: 
            print('Não foi encontrado nenhum revendedor.')


if __name__ == "__main__":
    asyncio.run(delete_popsicle(2))
    # asyncio.run(delete_dealer(2))