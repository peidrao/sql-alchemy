import asyncio
from random import randint
from conf.db_session import create_session
from models.models import Additive, Dealer, Flavor, Ingredient, Invoice, Lot, PackingType, Popsicle, PopsicleType, Preservative


async def insert_additive() -> Additive:
    random = randint(1, 1_000)
    name: str = f'Name {random}'
    chemical_formula: str = f'Chemical {random}'
    additive: Additive = Additive(name=name, chemical_formula=chemical_formula)

    async with create_session() as session:
        session.add(additive)
        await session.commit()
    
    print(f'ID: {additive.id} | Created At: {additive.created_at} | Name: {additive.name}')
    return additive


async def insert_flavor() -> Flavor:
    random = randint(1, 1_000)
    name: str = f'Flavor {random}'
    flavor: Flavor = Flavor(name=name)

    async with create_session() as session:
        session.add(flavor)
        await session.commit()
    
    return flavor


async def insert_packing_type() -> PackingType:
    random = randint(1, 1_000)
    name: str = f'Packing Type: {random}'
    packing: PackingType = PackingType(name=name)

    async with create_session() as session:
        session.add(packing)
        await session.commit()
    
    print(f'ID: {packing.id} | Created At: {packing.created_at} | Name: {packing.name}')

    return packing


async def insert_popsicle_type() -> PopsicleType:
    random = randint(1, 1_000)
    name: str = f'Popsicle Type: {random}'
    popsicle_type: PopsicleType = PopsicleType(name=name)

    async with create_session() as session:
        session.add(popsicle_type)
        await session.commit()
    
        return popsicle_type


async def insert_ingredient() -> Ingredient:
    random = randint(1, 1_000)
    name: str = f'Ingredient: {random}'
    ingredient: Ingredient = Ingredient(name=name)

    async with create_session() as session:
        session.add(ingredient)
        await session.commit()
    
    print(f'ID: {ingredient.id} | Created At: {ingredient.created_at} | Name: {ingredient.name}')

    return ingredient


async def insert_preservative() -> Preservative:
    random = randint(1, 1_000)
    name: str = f'Preservative - Name: {random}'
    description: str = f'Preservative - Description: {random}'
    preservative: Preservative = Preservative(name=name, description=description)

    async with create_session() as session:
        session.add(preservative)
        await session.commit()
    
    print(f'ID: {preservative.id} | Created At: {preservative.created_at} | Name: {preservative.name}')
    return preservative


async def insert_dealer() -> Dealer:
    random = randint(1, 1_000)
    name: str = f'Dealer - Name: {random}'
    corporate_name: str = f'Dealer - Corporate Name: {random}'
    contact: str = f'Dealer - Contact: {random}'

    dealer: Dealer = Dealer(name=name, corporate_name=corporate_name, contact=contact)

    async with create_session() as session:
        session.add(dealer)
        await session.commit()
    return dealer


async def insert_lot(type_popsice_id: int = None) -> Lot:
    random = randint(1, 1_000)
    quantity: int = random
    
    lot: Lot = Lot(quantity=quantity, types_popsicle_id=type_popsice_id)

    async with create_session() as session:
        session.add(lot)
        await session.commit()
        return lot


async def insert_invoice() -> Invoice:
    random = randint(1_000, 5_000)

    value: float = 1_000 + float(random)  
    description: str = f'New Invoice # {random}'
    serial_number: str = f'Serial # {random}'
    dealer : Dealer = await insert_dealer()
    invoice: Invoice = Invoice(value=value, serial_number=serial_number, description=description, dealer=dealer)
    
    type_popsicle = await insert_popsicle_type()
    lot2 = await insert_lot(type_popsicle.id)
    invoice.lots.append(lot2)
    async with create_session() as session:
        session.add(invoice)
        await session.commit()
        await session.refresh(invoice)
        return invoice



async def insert_popsicle() -> None:
    random = randint(10, 35)

    price: float = 10 + float(random)  
    name: str = f'Flavor:  {random}'

    flavor = await insert_flavor()
    packing_type = await insert_packing_type()
    popsicle_type = await insert_popsicle_type()

    popsicle: Popsicle = Popsicle(price=price, name=name, flavor_id=flavor, type_packing=packing_type,
                                type_popsicle=popsicle_type)
    
    ingredient1 = await insert_ingredient()
    ingredient2 = await insert_ingredient()
    preservative1 = await insert_preservative()
    preservative2 = await insert_preservative()
    additive1 = await insert_additive() 
    additive2 = await insert_additive() 

    popsicle.ingredients.append(ingredient1)
    popsicle.ingredients.append(ingredient2)
    popsicle.preservatives.append(preservative1)
    popsicle.preservatives.append(preservative2)
    popsicle.additions.append(additive1)
    popsicle.additions.append(additive2)

    async with create_session() as session:
        session.add(popsicle)
        await session.commit()
        await session.refresh(popsicle)
    
        return popsicle


if __name__ == '__main__':
    asyncio.run(insert_additive()) 
    # flavor = asyncio.run(insert_flavor())
    # packing_type = asyncio.run(insert_packing_type())
    # popsicle_type = asyncio.run(insert_popsicle_type())
    # asyncio.run(insert_ingredient())
    # asyncio.run(insert_preservative())
    # dealer = asyncio.run(insert_dealer())
    # lot = asyncio.run(insert_lot(1))
    # invoice = asyncio.run(insert_invoice())
    # asyncio.run(insert_popsicle())