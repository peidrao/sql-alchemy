from typing import List, Optional
from sqlalchemy import Column, BigInteger, DateTime, Integer, String, ForeignKey, DECIMAL, Table
from datetime import datetime
from models.model_base import ModelBase
from sqlalchemy.orm import relationship


class Additive(ModelBase):
    __tablename__: str = 'additions'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)
    chemical_formula: str = Column(String(100), nullable=True)

    def __repr__(self) -> str:
        return f'<Additive: {self.name}>'


class Flavor(ModelBase):
    __tablename__: str = 'flavors'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)

    def __repr__(self) -> str:
        return f'<Flavor: {self.name}>'


class PackingType(ModelBase):
    __tablename__: str = 'types_packing'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)

    def __repr__(self) -> str:
        return f'<PackingType: {self.name}>'


class PopsicleType(ModelBase):
    __tablename__: str = 'types_popsicle'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)

    def __repr__(self) -> str:
        return f'<PopsicleType: {self.name}>'


class Ingredient(ModelBase):
    __tablename__: str = 'ingredients'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)

    def __repr__(self) -> str:
        return f'<Ingreditent: {self.name}>'


class Preservative(ModelBase):
    __tablename__: str = 'preservatives'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)
    description: str = Column(String(45), unique=True, nullable=False)

    def __repr__(self) -> str:
        return f'<Preservative: {self.name}>'


class Dealer(ModelBase):
    __tablename__: str = 'dealers'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True, nullable=False)
    corporate_name: str = Column(String(45), nullable=True)
    contact: str = Column(String(45), nullable=True)

    def __repr__(self) -> str:
        return f'<Dealer: {self.name}>'


class Lot(ModelBase):
    __tablename__: str = 'lots'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    types_popsicle_id: int = Column(Integer, ForeignKey('types_popsicle.id'))
    types_popsicle: PopsicleType = relationship('PopsicleType', lazy='joined')
    quantity : int = Column(Integer, nullable=False)
    

    def __repr__(self) -> str:
        return f'<Lot: {self.id}>'


lots_invoice = Table(
    'lots_invoice',
    ModelBase.metadata,
    Column('id_invoice', Integer, ForeignKey('invoices.id')),
    Column('id_lot', Integer, ForeignKey('lots.id', ondelete="CASCADE"))
)


class Invoice(ModelBase):
    __tablename__: str = 'invoices'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    value: float = Column(DECIMAL(8, 2), nullable=False)
    serial_number: str = Column(String(45), unique=True, nullable=False)
    description: str = Column(String(200), unique=True, nullable=False)

    dealer_id: int = Column(Integer, ForeignKey('dealers.id', ondelete="CASCADE"))
    dealer: Dealer = relationship('Dealer', lazy='joined', cascade="delete")

    lots: List[Lot] = relationship('Lot', secondary=lots_invoice, backref='lot', lazy='dynamic', cascade="delete")

    def __repr__(self) -> str:
        return f'<Invoice: {self.id}>'


ingredients_popsicles = Table(
    'ingredients_popsicles',
    ModelBase.metadata,
    Column('id_popsicle', Integer, ForeignKey('popsicles.id')),
    Column('id_ingredient', Integer, ForeignKey('ingredients.id'))
)

preservatives_popsicles = Table(
    'preservatives_popsicles',
    ModelBase.metadata,
    Column('id_popsicle', Integer, ForeignKey('popsicles.id')),
    Column('id_preservative', Integer, ForeignKey('preservatives.id'))
)

additions_popsicles = Table(
    'additions_popsicles',
    ModelBase.metadata,
    Column('id_popsicle', Integer, ForeignKey('popsicles.id')),
    Column('id_additive', Integer, ForeignKey('additions.id'))
)


class Popsicle(ModelBase):
    __tablename__: str = 'popsicles'

    id: int = Column(BigInteger, primary_key=True, autoincrement=True)
    created_at: datetime = Column(DateTime, default=datetime.now, index=True)
    name: str = Column(String(45), unique=True)
    price: float = Column(DECIMAL(8, 2), nullable=False)

    flavor_id: int = Column(Integer, ForeignKey('flavors.id'))
    flavor: Flavor = relationship('Flavor', lazy='joined')

    type_popsicle_id: int = Column(Integer, ForeignKey('types_popsicle.id'))
    type_popsicle: PopsicleType = relationship('PopsicleType', lazy='joined')

    type_packing_id: int = Column(Integer, ForeignKey('types_packing.id'))
    type_packing: PackingType = relationship('PackingType', lazy='joined')

    ingredients: List[Ingredient] = relationship('Ingredient', secondary=ingredients_popsicles, backref='ingredient', lazy='joined')
    preservatives: Optional[List[Preservative]] = relationship('Preservative', secondary=preservatives_popsicles, backref='preservative', lazy='joined')
    additions: Optional[List[Additive]] = relationship('Additive', secondary=additions_popsicles, backref='addition', lazy='joined')
    
    def __repr__(self) -> str:
        return f'<Popsicle: {self.id}>'
