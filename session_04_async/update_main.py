import asyncio
from sqlalchemy.future import select
from conf.db_session import create_session

from models.models import Flavor, Popsicle


async def update_flavor(flavor_id: int, new_name: str) -> Flavor:
    async with create_session() as session:
        
        flavor: Flavor = await session.execute(select(Flavor).filter(Flavor.id == flavor_id))
        flavor = flavor.scalar_one_or_none()

        if flavor:
            flavor.name = new_name
            await session.commit()
            return flavor
        else:
            print('Não existe esse sabor')


async def update_popsicle(popsicle_id: int, new_flavor_id: int, new_price: float, new_name: str) -> None:
    async with create_session() as session:
        
        popsicle: Popsicle = await session.execute(select(Popsicle).filter(Popsicle.id == popsicle_id))
        popsicle = popsicle.unique().scalar_one_or_none()

        if popsicle:
            popsicle.name = new_name
            popsicle.flavor_id = new_flavor_id
            popsicle.price = new_price
            await session.commit()
            return popsicle
        else:
            print('Não existe picolé com esse ID')
        


if __name__ == '__main__':
    # asyncio.run(update_flavor(flavor_id=1, new_name='Novo sabor'))
    asyncio.run(update_popsicle(1, 1, 50.99, 'Novo picolé'))